package com.citi.training.products.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.products.model.Product;
import com.citi.training.products.repo.ProductRepository;

@RestController
@RequestMapping("/products")
public class ProductsController {
	
	private static final Logger LOG = LoggerFactory.getLogger(ProductsController.class);

	@Autowired
	ProductRepository productRepo; // bean stuff

	@RequestMapping(method = RequestMethod.GET) // if we get a GET, execute this function
	public List<Product> findAll() {
		LOG.info("HTTP GET to findAll()");
		LOG.debug("This is a much more verbose debugging message");
		return productRepo.findAll();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public Product save(@RequestBody Product product) {
		productRepo.save(product);
		return product;
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Product findbyId(@PathVariable int id) {
		return productRepo.findById(id);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public void removebyId(@PathVariable int id) {
		allProducts.remove(id);
	}
	
}
